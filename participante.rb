class Participante

  attr_accessor :dni, :nombre, :edad, :puntaje_jurado, :puntaje_publico

  def initialize dni, nombre, edad, puntaje_jurado, puntaje_publico
    @dni, @nombre, @edad = dni, nombre, edad
    @puntaje_jurado, @puntaje_publico = puntaje_jurado, puntaje_publico
  end

  def mostrar
    "Concursante #{nombre}, con DNI #{dni}. Puntaje del jurado: #{puntaje_jurado}. Puntaje del publico #{puntaje_publico}."
  end

  def puntaje_final
    puntaje_jurado * 0.7 + puntaje_publico * 0.3
  end

end

class Amateur < Participante

  def initialize dni, nombre, edad, puntaje_jurado, puntaje_publico
    super dni, nombre, edad, puntaje_jurado, puntaje_publico
  end

  def mostrar 
    super + "Categoria: Amateur. Puntaje final: #{puntaje_final}"
  end

end

class Profesional < Participante

  attr_accessor :años_exp
    
  def initialize dni, nombre, edad, puntaje_jurado, puntaje_publico, años_exp
    super dni, nombre, edad, puntaje_jurado, puntaje_publico
      @años_exp = años_exp
  end

  def puntaje_final
    if años_exp  > 10 
      super + 2
    else
      super
    end
  end
    
  def mostrar 
    super + "Categoria: Profesional. Puntaje final: #{puntaje_final}"
  end
    
end

class Master < Participante
    
  attr_accessor :años_exp
        
  def initialize dni, nombre, edad, puntaje_jurado, puntaje_publico, años_exp
    super dni, nombre, edad, puntaje_jurado, puntaje_publico
    @años_exp = años_exp
  end

  def puntaje_final
    puntaje_jurado
  end
        
  def mostrar 
    super + "Categoria: Master. Puntaje final: #{puntaje_final}"
  end
        
end