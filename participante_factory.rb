class ParticipanteFactory

  def self.crear_participantes categoria, dni, nombre, edad, puntaje_jurado, puntaje_publico, años_exp
    case categoria
      when "amateur"
        Amateur.new dni, nombre, edad, puntaje_jurado, puntaje_publico
      when "profesional"
        Profesional.new dni, nombre, edad, puntaje_jurado, puntaje_publico, años_exp
      when "master"
        Master.new dni, nombre, edad, puntaje_jurado, puntaje_publico, años_exp
    end
  end

end

