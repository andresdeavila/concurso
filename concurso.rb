class Concurso

  attr_accessor :nombre, :participantes

  def initialize nombre
    @nombre = nombre
    @participantes = Array.new
  end

  def inscribir_participante categoria, dni, nombre, edad, puntaje_jurado, puntaje_publico, años_exp = nil
    participantes.push(ParticipanteFactory.crear_participantes categoria, dni, nombre, edad, puntaje_jurado, puntaje_publico, años_exp)
  end

  def listar_concursantes
    msj = "Concurso #{nombre}\n"
    msj += "Lista de concursantes:\n"
    @participantes.each do |part|
      msj += part.mostrar + "\n"
    end
    msj += "Total de concursantes: #{participantes.size}"
  end

  def ganador_general 
    puntaje_mayor= 0
    ganador = nil
    msj = "Ganador general del concurso:\n"
    @participantes.each do |part|
      if part.puntaje_final > puntaje_mayor
        puntaje_mayor = part.puntaje_final
        ganador = part
      end
    end
    msj += "#{ganador.nombre} con puntaje final #{puntaje_mayor}"
  end

  def ganador_por_categoria
    puntaje_mayor_amateur = 0
    puntaje_mayor_profesional = 0
    puntaje_mayor_master = 0

    amateur_ganador = nil
    profesional_ganador = nil
    master_ganador = nil

    msj = "Ganadores por categoria:\n"
    @participantes.each do |part|
      case part.class.to_s
        when "Amateur" 
          if part.puntaje_final > puntaje_mayor_amateur 
            puntaje_mayor_amateur = part.puntaje_final
            amateur_ganador = part
          end
        when "Profesional" 
          if part.puntaje_final > puntaje_mayor_profesional 
            puntaje_mayor_profesional = part.puntaje_final
            profesional_ganador = part
          end
        when "Master" 
          if part.puntaje_final > puntaje_mayor_master
            puntaje_mayor_master = part.puntaje_final
            master_ganador = part
          end
      end
    end  
    msj += "Categoria Amateur: #{amateur_ganador.nombre}, PF: #{puntaje_mayor_amateur}.\n"
    msj += "Categoria Profesional: #{profesional_ganador.nombre}, PF: #{puntaje_mayor_profesional}.\n"
    msj += "Categoria Amateur: #{master_ganador.nombre}, PF: #{puntaje_mayor_master}.\n"
  end
end

           
