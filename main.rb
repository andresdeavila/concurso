require_relative './concurso'
require_relative './participante_factory'
require_relative './participante'

concurso = Concurso.new "Probando"
concurso.inscribir_participante("amateur", "1", "Juan Perez", 20, 75, 95)
concurso.inscribir_participante("amateur", "12", "Carlos Olmos", 30, 55, 78)
concurso.inscribir_participante("profesional", "123", "Miguel Benitez", 35, 87, 76, 11)
concurso.inscribir_participante("profesional", "1234", "Andres Espejo", 40, 70, 46, 9)
concurso.inscribir_participante("master", "12345", "Luis Gonzales", 50, 50, 60)
concurso.inscribir_participante("master", "123456", "Jorge Faropa", 70, 90, 50)

puts concurso.listar_concursantes
puts concurso.ganador_general
puts concurso.ganador_por_categoria
